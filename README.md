# OpenRGB_cmake



## Content of this repository

Due to reasons I don't understand the synchronisation from the original openrgb repository
to my previous repository copy of openrgb has stopped in mid 2022.

While the only different was the usage of a CMakeList.txt to compile the whole
project with cmake I decided to setup a new repository containing only those
file. You have to only put that file into the original openrgb repository checked out folder
and build/setup the cmake as usual.

original repository: https://gitlab.com/CalcProgrammer1/OpenRGB

## Related project


## ![OpenRGB](https://gitlab.com/CalcProgrammer1/OpenRGB/-/wikis/uploads/5b7e633ac9f63b00c8a4c72686206c3f/OpenRGB.png "OpenRGB Logo")

![Visitors](https://visitor-badge.glitch.me/badge?page_id=org.openrgb.readme) ![Pipeline Status](https://gitlab.com/CalcProgrammer1/OpenRGB/badges/master/pipeline.svg)

Visit our website at https://openrgb.org!
